extends VBoxContainer


var _command_entry_template = preload('res://components/_debug/dev_console/dev_console_entry.tscn')


@onready var _keep_log_checkbox = %KeepLogCheckBox
@onready var _clear_button = %ClearButton
@onready var _command_container = %CommandContainer
@onready var _command_edit: LineEdit = %CommandEdit


var _logger: Logger = LOGGING.get_logger('DevConsole')

var _added_commands: Dictionary = {}


func _ready():
	_clear_button.pressed.connect(clear_command_log)
	_command_edit.text_submitted.connect(_on_text_entered)

	_command_edit.text = ''


func grab_focus_for_cmd_edit() -> void:
	_command_edit.grab_focus()


func add_console_cmd(__cmd: String, __f: Callable) -> void:
	_added_commands[__cmd] = __f


func add_log_msg(__text: String, __color: Color = Color.WHITE) -> void:
	if __text.contains(_logger.get_name()):
		return # ignore log msg of the dev console itself

	_fade_last_entry()

	var label = Label.new()
	label.set_text(__text)
	label.set_modulate(__color)
	label.set_autowrap_mode(TextServer.AUTOWRAP_WORD_SMART)
	label.size_flags_horizontal = Control.SIZE_EXPAND_FILL

	_command_container.add_child(label)
	_fade_last_entry()
	_scroll_to_bottom()


func clear() -> void:
	_added_commands = {}
	
	if not _keep_log_checkbox.button_pressed:
		clear_command_log()


func clear_command_log() -> void:
	for child in _command_container.get_children():
		_command_container.remove_child(child)
		child.queue_free()


func clean_up_for_hide():
	_command_edit.set_text('')
	_fade_last_entry()


func _on_text_entered(__raw_command: String):
	_logger.info('Executed command %s'%[__raw_command])

	var split_command = __raw_command.split(' ')
	var command = split_command[0]
	var args = split_command.slice(1)
	var command_as_func_name = '__cmd_%s'%command

	var new_entry = _command_entry_template.instantiate()

	var was_handled = false
	var result = null
	if command in _added_commands:
		result = _added_commands[command].call(args)
		was_handled = true
	elif has_method(command_as_func_name):
		result = call(command_as_func_name, args)
		was_handled = true

	var was_success = false
	if was_handled:
		if result:
			if typeof(result) == TYPE_STRING:
				was_success = true
				new_entry.set_data_result(__raw_command, result)
			elif result.has('is_error'):
				new_entry.set_data_error(__raw_command, result.get('message', 'Error'))
			else:
				was_success = true
				if 'message' in result:
					new_entry.set_data_result(__raw_command, result.get('message'))
				else:
					new_entry.set_data_ok(__raw_command)
		else:
			was_success = true
			new_entry.set_data_ok(__raw_command)
	else:
		new_entry.set_data_error(__raw_command, 'Command not defined!')

	if was_success:
		_command_edit.text = ''

	_fade_last_entry()
	_command_container.add_child(new_entry)
	_scroll_to_bottom()


func _fade_last_entry():
	if _command_container.get_child_count() > 0:
		var current_modulate = _command_container.get_child(-1).modulate
		_command_container.get_child(-1).modulate = Color(
			current_modulate.r,
			current_modulate.g,
			current_modulate.b,
			0.6
		)


func _scroll_to_bottom():
	var tree = get_tree()
	if not tree:
		return

	await tree.process_frame

	var scroll_container: ScrollContainer = _command_container.get_parent()
	scroll_container.scroll_vertical = (scroll_container.get_node('_v_scroll') as ScrollBar).max_value as int


func __cmd_clear(__args):
	for child in _command_container.get_children():
		child.queue_free()


func __cmd_help(__args):
	var command_list: String = 'available commands:'
	for added_cmd in _added_commands.keys():
		command_list += '\n  ' + added_cmd
	for method in get_method_list():
		if method.name.begins_with('__cmd_'):
			command_list += '\n  ' + method.name.trim_prefix('__cmd_')
	return command_list


# Add custom commands below:


func __cmd_echo(__args):
	if __args.size() != 1:
		return { is_error=true, message='invalid arguments! expected: <text>' }

	return __args[0]


func __cmd_get_save_id(__args):
	var id = STATE.get_current_save_id()
	return id if id else '<no save loaded>'
