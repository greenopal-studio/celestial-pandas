extends Node


func set_data_ok(__command: String):
	$CommandLabel.text = __command
	$ResultLabel.hide()
	$ErrorLabel.hide()


func set_data_result(__command: String, __result: String):
	$CommandLabel.text = __command
	$OkLabel.hide()
	$ResultLabel.text = __result
	$ErrorLabel.hide()


func set_data_error(__command: String, __error: String):
	$CommandLabel.text = __command
	$OkLabel.hide()
	$ResultLabel.hide()
	$ErrorLabel.text = __error
