extends VBoxContainer


@onready var _buttons_container = %ButtonsContainer


func add_action_button(__text: String, __f: Callable) -> void:
	var button = Button.new()
	button.set_text(__text)
	button.pressed.connect(__f)
	_buttons_container.add_child(button)


func clear() -> void:
	for child in _buttons_container.get_children():
		_buttons_container.remove_child(child)
		child.queue_free()
