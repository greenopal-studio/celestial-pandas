extends PanelContainer


@export var title := 'Title' :
	set(__title):
		var title_label = %TitleLabel
		if title_label:
			title_label.set_text(__title)


@onready var _viewport := get_viewport()


var _last_mouse_pos := Vector2.INF


func _input(__event):
	if not is_visible_in_tree():
		return

	var current_mouse_pos = _viewport.get_mouse_position()

	if __event is InputEventMouseButton:
		if (
			_last_mouse_pos == Vector2.INF and __event.is_pressed()
			and UtilService.is_pos_inside_rect(
				current_mouse_pos,
				get_global_position(), get_global_position() + get_size()
			)
		):
			_viewport.set_input_as_handled()
			_last_mouse_pos = current_mouse_pos
		elif _last_mouse_pos != Vector2.INF and not __event.is_pressed():
			_viewport.set_input_as_handled()
			_last_mouse_pos = Vector2.INF

	elif __event is InputEventMouseMotion:
		var parent = get_parent()
		var parent_pos = parent.get_global_position()
		if _last_mouse_pos != Vector2.INF:
			_viewport.set_input_as_handled()
			var target_pos = parent_pos + (current_mouse_pos - _last_mouse_pos)
			if target_pos.x < 0:
				target_pos.x = 0
			if target_pos.y < 0:
				target_pos.y = 0
			if target_pos.x > _viewport.get_size().x - parent.get_size().x:
				target_pos.x = _viewport.get_size().x - parent.get_size().x
			if target_pos.y > _viewport.get_size().y - parent.get_size().y:
				target_pos.y = _viewport.get_size().y - parent.get_size().y
			parent.set_global_position(target_pos)
			_last_mouse_pos = current_mouse_pos
