extends VBoxContainer


const ONE_SHOT_TIMEOUT = 2.0


var _label_scene = preload('res://components/_debug/values/value_label.tscn')

@onready var _labels_container = %LabelsContainer


var _added_values: Array[Dictionary] = []


func _physics_process(__delta):
	for entry in _added_values:
		var result = entry['expression'].execute([], entry['obj'], false)
		if entry['expression'].has_execute_failed():
			result = null
		entry['label_node'].set_data(entry['label_text'], result)


func add_value_header(__label_text: String) -> void:
	var label = Label.new()
	_labels_container.add_child(label)
	label.set_text(__label_text)


func add_value(__label_text: String, __obj: Variant, __expression_str: String) -> void:
	var expression = Expression.new()
	expression.parse(__expression_str)

	var label_node = _label_scene.instantiate()
	_labels_container.add_child(label_node)

	_added_values.append({
		label_text = __label_text,
		obj = __obj,
		expression = expression,
		label_node = label_node
	})


func one_shot_value(__text: String) -> void:
	var label = Label.new()
	_labels_container.add_child(label)
	label.set_modulate(Color.PALE_GREEN)
	label.set_text(__text)

	await get_tree().create_timer(ONE_SHOT_TIMEOUT).timeout

	if not label:
		return # scene got free'd in the meantime

	_labels_container.remove_child(label)
	if _labels_container.get_child_count() == 0:
		hide()
	label.queue_free()


func clear() -> void:
	_added_values = []
	for child in _labels_container.get_children():
		_labels_container.remove_child(child)
		child.queue_free()
