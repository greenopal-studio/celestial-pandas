extends Control


@onready var _okay_button = %OkayButton
@onready var _text_label = %TextLabel


func _ready():
	_okay_button.pressed.connect(func(): queue_free())
	
	
func set_text(__text: String):
	_text_label.set_text(__text)
