extends CenterContainer


@onready var _input_container: Node = %InputContainer
@onready var _input_label: RichTextLabel = %InputTextLabel
@onready var _hint_container: Node = %HintContainer
@onready var _hint_label: RichTextLabel = %HintTextLabel
@onready var _dialogue_container: Node = %DialogueContainer
@onready var _dialogue_label: RichTextLabel = %DialogueTextLabel


var _has_input_thought := false
var _hint_thoughts_queue: Array[String] = []
var _dialogue_thoughts_queue: Array[String] = []


func _ready():
	STATE.add_hint_thoughts.connect(_on_add_hint_thoughts)
	STATE.add_dialogue_thoughts.connect(_on_add_dialogue_thoughts)
	STATE.clear_thoughts.connect(clear_thought)

	_input_container.hide()
	_hint_container.hide()
	_dialogue_container.hide()


func _input(__event: InputEvent):
	if __event is InputEventKey and Input.is_action_just_pressed('thought_discard'):
		if _hint_container.visible:
			if len(_hint_thoughts_queue) > 0:
				set_hint_thought(_hint_thoughts_queue.pop_front())
			else:
				_hint_container.hide()
				if _has_input_thought and not _input_container.visible:
					_input_container.show()
				STATE.done_with_thoughts.emit()

		elif _dialogue_container.visible:
			if len(_dialogue_thoughts_queue) > 0:
				set_dialogue_thought(_dialogue_thoughts_queue.pop_front())
			else:
				_dialogue_container.hide()
				if _has_input_thought and not _input_container.visible:
					_input_container.show()
				STATE.done_with_thoughts.emit()


func _on_add_hint_thoughts(__thoughts: Array) -> void:
	if len(__thoughts) < 1:
		return

	set_hint_thought(__thoughts[0])

	_hint_thoughts_queue.append_array(__thoughts.slice(1))


func _on_add_dialogue_thoughts(__thoughts: Array) -> void:
	if len(__thoughts) < 1:
		return

	set_dialogue_thought(__thoughts[0])

	_dialogue_thoughts_queue.append_array(__thoughts.slice(1))


func set_input_thought(__action: String, __action_color: Color, __post_text: String) -> void:
	var event = InputMap.action_get_events(__action)[0]
	var keycode = DisplayServer.keyboard_get_keycode_from_physical(event.physical_keycode)
	var key_string = OS.get_keycode_string(keycode)
	
	var full_text = '[center][color=#%s][wave]Press %s[/wave][/color] to %s'%[
		__action_color.to_html(),
		key_string,
		__post_text
	]
	_input_label.set_text(full_text)

	_hint_container.hide()
	_dialogue_container.hide()
	_input_container.show()

	_hint_thoughts_queue = []
	_dialogue_thoughts_queue = []

	_has_input_thought = true


func clear_input_thought() -> void:
	_input_container.hide()

	_has_input_thought = false


func clear_thought() -> void:
	_hint_container.hide()
	_dialogue_container.hide()

	_hint_thoughts_queue = []
	_dialogue_thoughts_queue = []


func set_hint_thought(__text: String) -> void:
	_hint_label.set_text('[center]%s'%__text)

	_input_container.hide()
	_dialogue_container.hide()
	_hint_container.show()


func set_dialogue_thought(__text: String) -> void:
	_dialogue_label.set_text('[center]%s'%__text)

	_hint_container.hide()
	_input_container.hide()
	_dialogue_container.show()
