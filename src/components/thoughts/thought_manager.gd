extends Node


var _init_hints = [
	'What just happened? Where am I?',
	'I should probably explore these mysterious, floating islands to see what\'s going on...',
	'Oh, there is another red panda over there, maybe they know more about this place.',
]

var _star_fruit_hints = [
	'That is a very nice fruit. Tastes... [wave]magical...[/wave]',
	'I should try to put it inside the mythical cave'
]
var _has_thought_star_fruit := false

var _retry_sacrifice_hints = [
	'Hmh, no [wave]summoning[/wave] happened... The island gods did not accept my sacrifice...',
	'The drawings on the cave wall seem connected. Maybe the runes want a different item...'
]
var _has_thought_retry_sacrifice := false


func _ready():
	STATE.add_inventory_item.connect(_on_add_inventory_item)
	STATE.end_day.connect(_on_end_day)

	# add initial hints:
	await get_tree().process_frame
	STATE.add_hint_thoughts.emit(_init_hints)


func _on_add_inventory_item(__item_id: ID.ITEM_TYPES):
	if __item_id == ID.ITEM_TYPES.STAR_FRUIT and not _has_thought_star_fruit:
		STATE.add_hint_thoughts.emit(_star_fruit_hints)
		_has_thought_star_fruit = true


func _on_end_day():
	if STATE.get_sacrifice_tries() > 0 and not _has_thought_retry_sacrifice:
		await get_tree().create_timer(1.0).timeout # because of fade of day screen
		STATE.add_hint_thoughts.emit(_retry_sacrifice_hints)
		_has_thought_retry_sacrifice = true
