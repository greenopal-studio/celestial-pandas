extends HBoxContainer


@onready var _volume_slider: HSlider = $VolumeSlider
@onready var _value_label: Label = $Value


var _current_settings: SettingsData
var _settings_scene: SettingsScene


func _ready():
	_volume_slider.value_changed.connect(_on_volume_changed)


func set_data(__current_settings: SettingsData, __settings_scene: SettingsScene) -> void:
	_current_settings = __current_settings
	_settings_scene = __settings_scene
	_set_current_volume(_current_settings.get_volume())


func _on_volume_changed(__new_volume: float) -> void:
	_value_label.set_text('%d%%' % [__new_volume + 80])
	
	var __was_different = false
	if _current_settings.get_volume() != SETTINGS.get_data().get_volume():
		__was_different = true
	
	var __was_changed = _current_settings.set_volume(__new_volume)
	if __was_changed:
		if _current_settings.get_volume() == SETTINGS.get_data().get_volume():
			_settings_scene.remove_setting_changed(ID.SETTING_TYPES.VOLUME)
		elif not __was_different:
			_settings_scene.add_setting_changed(ID.SETTING_TYPES.VOLUME)


func _set_current_volume(__new_volume: float) -> void:
	_volume_slider.set_value(__new_volume)
