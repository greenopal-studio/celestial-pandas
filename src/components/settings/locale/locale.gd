extends HBoxContainer


const DEFAULT_LOCALE = 'en'


@onready var _dropdown_button = $'%DropdownButton'


var _loaded_locales: PackedStringArray
var _current_settings: SettingsData
var _settings_scene: SettingsScene


func _ready():
	var __os_locale = OS.get_locale()
	_loaded_locales = TranslationServer.get_loaded_locales()
	
	if __os_locale in _loaded_locales:
		_dropdown_button.add_item(TranslationServer.get_locale_name(__os_locale))
		_dropdown_button.set_item_metadata(_dropdown_button.get_item_count() - 1, __os_locale)
	else:
		var __split_os_locale = __os_locale.split('_')
		if __split_os_locale[0] in _loaded_locales:
			_dropdown_button.add_item(TranslationServer.get_locale_name(__split_os_locale[0]))
			_dropdown_button.set_item_metadata(_dropdown_button.get_item_count() - 1, __split_os_locale[0])
		else:
			_dropdown_button.add_item(TranslationServer.get_locale_name(DEFAULT_LOCALE))
			_dropdown_button.set_item_metadata(_dropdown_button.get_item_count() - 1, DEFAULT_LOCALE)
	
	_dropdown_button.add_separator()
	
	for __loaded_locale in _loaded_locales:
		if __loaded_locale != _dropdown_button.get_item_metadata(0):
			_dropdown_button.add_item(TranslationServer.get_locale_name(__loaded_locale))
			_dropdown_button.set_item_metadata(_dropdown_button.get_item_count() - 1, __loaded_locale)
	
	_dropdown_button.item_selected.connect(_on_locale_selected)


func set_data(__current_settings: SettingsData, __settings_scene: SettingsScene) -> void:
	_current_settings = __current_settings
	_settings_scene = __settings_scene
	_set_current_locale(_current_settings.get_locale())


func _set_current_locale(__current_locale: String) -> void:
	if __current_locale in _loaded_locales:
		for __i in _dropdown_button.get_item_count():
			if _dropdown_button.get_item_metadata(__i) == __current_locale:
				_dropdown_button.select(__i)
				break
	else:
		var __split_current_locale = __current_locale.split('_')
		if __split_current_locale[0] in _loaded_locales:
			for __i in _dropdown_button.get_item_count():
				if _dropdown_button.get_item_metadata(__i) == __split_current_locale[0]:
					_dropdown_button.select(__i)
					break
		else:
			for __i in _dropdown_button.get_item_count():
				if _dropdown_button.get_item_metadata(__i) == DEFAULT_LOCALE:
					_dropdown_button.select(__i)
					break


func _get_current_locale() -> String:
	return _dropdown_button.get_item_metadata(_dropdown_button.get_selected())


func _on_locale_selected(__locale_index: int) -> void:
	var __was_different = false
	if _current_settings.get_locale() != SETTINGS.get_data().get_locale():
		__was_different = true
	
	var __was_changed = _current_settings.set_locale(_dropdown_button.get_item_metadata(__locale_index))
	if __was_changed:
		if _current_settings.get_locale() == SETTINGS.get_data().get_locale():
			_settings_scene.remove_setting_changed(ID.SETTING_TYPES.LOCALE)
		elif not __was_different:
			_settings_scene.add_setting_changed(ID.SETTING_TYPES.LOCALE)
