extends VBoxContainer


@onready var _invert_x_check: CheckButton = $'%InvertXCheckButton'
@onready var _invert_y_check: CheckButton = $'%InvertYCheckButton'
@onready var _joy_stick_mode_option: OptionButton = $'%JoyStickModeOptionButton'

var _current_settings: SettingsData
var _settings_scene: SettingsScene


func _ready():
	_invert_x_check.toggled.connect(_on_invert_x_changed)
	_invert_y_check.toggled.connect(_on_invert_y_changed)
	_joy_stick_mode_option.item_selected.connect(_on_joy_stick_mode_changed)

	_joy_stick_mode_option.add_item(tr('left stick = move, right stick = rotate'))
	_joy_stick_mode_option.add_item(tr('left stick = rotate, right stick = move'))


func set_data(__current_settings: SettingsData, __settings_scene: SettingsScene) -> void:
	_current_settings = __current_settings
	_settings_scene = __settings_scene
	
	_set_invert_x(_current_settings.get_camera_invert_x())
	_set_invert_y(_current_settings.get_camera_invert_y())
	_set_joy_stick_mode(_current_settings.get_camera_joy_stick_mode())


func _on_invert_x_changed(__new_value: bool) -> void:
	var __was_different = \
		_current_settings.get_camera_invert_x() != SETTINGS.get_data().get_camera_invert_x()
	
	var __was_changed = _current_settings.set_camera_invert_x(__new_value)
	if __was_changed:
		if _current_settings.get_camera_invert_x() == SETTINGS.get_data().get_camera_invert_x():
			_settings_scene.remove_setting_changed(ID.SETTING_TYPES.CAMERA_INVERT_X)
		elif not __was_different:
			_settings_scene.add_setting_changed(ID.SETTING_TYPES.CAMERA_INVERT_X)


func _set_invert_x(__new_value: bool) -> void:
	_invert_x_check.button_pressed = __new_value


func _on_invert_y_changed(__new_value: bool) -> void:
	var __was_different = \
		_current_settings.get_camera_invert_y() != SETTINGS.get_data().get_camera_invert_y()
	
	var __was_changed = _current_settings.set_camera_invert_y(__new_value)
	if __was_changed:
		if _current_settings.get_camera_invert_y() == SETTINGS.get_data().get_camera_invert_y():
			_settings_scene.remove_setting_changed(ID.SETTING_TYPES.CAMERA_INVERT_Y)
		elif not __was_different:
			_settings_scene.add_setting_changed(ID.SETTING_TYPES.CAMERA_INVERT_Y)


func _set_invert_y(__new_value: bool) -> void:
	_invert_y_check.button_pressed = __new_value


func _on_joy_stick_mode_changed(__new_value: int) -> void:
	var __was_different = \
		_current_settings.get_camera_joy_stick_mode() != SETTINGS.get_data().get_camera_joy_stick_mode()
	
	var __was_changed = _current_settings.set_camera_joy_stick_mode(__new_value)
	if __was_changed:
		if _current_settings.get_camera_joy_stick_mode() == SETTINGS.get_data().get_camera_joy_stick_mode():
			_settings_scene.remove_setting_changed(ID.SETTING_TYPES.CAMERA_JOY_STICK_MODE)
		elif not __was_different:
			_settings_scene.add_setting_changed(ID.SETTING_TYPES.CAMERA_JOY_STICK_MODE)


func _set_joy_stick_mode(__new_value: int) -> void:
	_joy_stick_mode_option.select(__new_value)
