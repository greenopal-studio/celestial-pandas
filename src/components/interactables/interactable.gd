@tool
class_name Interactable extends Node3D


const marker_scene = preload('res://components/interactables/marker.tscn')


var _marker_color: Color = Color.DARK_ORANGE
@export var marker_color: Color :
	set(__value):
		_marker_color = __value
		if _marker:
			_marker.color = __value
	get:
		return _marker_color

var _marker_size: float = 1.0
@export_range(0.1, 2.0) var marker_size: float :
	set(__value):
		_marker_size = __value
		if _marker:
			_marker.size = __value
	get:
		return _marker_size

var _thought_text
@export_multiline var thought_text: String = 'cause chaos' :
	set(__thought_text):
		_thought_text = __thought_text
	get:
		if _thought_text:
			return _thought_text
		else:
			return ''


var _marker: Node
var _is_focused := false


func _ready():
	if not Engine.is_editor_hint():
		STATE.end_day.connect(_on_end_day)

	_marker = marker_scene.instantiate()
	add_child(_marker)
	_marker.color = _marker_color
	_marker.size = _marker_size
	
	if not Engine.is_editor_hint():
		_marker.hide()


func _on_end_day() -> void:
	pass


func get_action_name() -> String:
	return 'interact'


func is_interactable() -> bool:
	return true


func focus():
	_marker.show()
	_is_focused = true
	

func unfocus():
	_marker.hide()
	_is_focused = false


func handle_interaction() -> void:
	pass # overriden by inheriting class
