@tool
extends InteractableItem


@onready var _full_state: Node3D = %FullStateModel


func is_interactable() -> bool:
	return _full_state.visible


func handle_interaction() -> void:
	super.handle_interaction()

	_full_state.hide()


func _on_end_day() -> void:
	_full_state.show()
