extends Interactable


@onready var _items_node := $Items
@onready var _star_fruit := $StarFruit


var _has_star_fruit := false


func _ready() -> void:
	super._ready()
	STATE.constellation_summoned.connect(_on_constellation_summoned)


func _on_constellation_summoned(__constellation: ID.CONSTELLATION_TYPES) -> void:
	reset()


func is_interactable() -> bool:
	return ID.ITEM_TYPES.STAR_FRUIT in STATE.get_inventory_items()


func reset() -> void:
	_has_star_fruit = false
	_star_fruit.hide()
	_star_fruit.set_process_mode(PROCESS_MODE_DISABLED)
	_thought_text = 'place down your star fruit'
	for __item_obj in _items_node.get_children():
		__item_obj.queue_free()


func put_item_on_table(__item_id: ID.ITEM_TYPES, __pos_id: int) -> void:
	var __obj_scene = DATA.items[__item_id].obj_scene.instantiate()
	var __marker_node = get_node('Marker3D' + str(__pos_id))
	if __marker_node != null:
		_items_node.add_child(__obj_scene)
		__obj_scene.set_position(__marker_node.get_position() + Vector3(0, 0.02, 0))
		__obj_scene.set_scale(Vector3(0.2, 0.2, 0.2))


func handle_interaction() -> void:
	if not _has_star_fruit:
		if not ID.ITEM_TYPES.STAR_FRUIT in STATE.get_inventory_items():
			return
		
		_has_star_fruit = true
		STATE.emit_signal('remove_inventory_item', ID.ITEM_TYPES.STAR_FRUIT)
		_star_fruit.set_process_mode(PROCESS_MODE_ALWAYS)
		_star_fruit.show()
		_thought_text = '...look at the star fruit'
		return
