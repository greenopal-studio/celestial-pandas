@tool
extends Interactable


const ROTATION_SPEED = 0.15


@export_multiline var initial_text: Array[String] = ['']
@export var teaches_punch: bool = false
@export var teaches_claw: bool = false
@export_multiline var quip_texts: Array[String] = ['']


@onready var _initial_rotation: float = rotation.y


var _is_in_dialogue := false


func _ready():
	super._ready()

	if not Engine.is_editor_hint():
		STATE.done_with_thoughts.connect(_on_done_with_thoughts)


func handle_interaction_with_player(__player_position: Vector3):
	_is_in_dialogue = true

	var direction = global_position.direction_to(__player_position)
	var target_angle = direction.signed_angle_to(Vector3.FORWARD, Vector3.UP) * -1.0
	_rotate(target_angle)

	if initial_text and len(initial_text[0]):
		STATE.add_dialogue_thoughts.emit(initial_text)

		if teaches_punch:
			STATE.learn_punch.emit()
		elif teaches_claw:
			STATE.learn_claw.emit()

		initial_text = []

	elif len(quip_texts) and len(quip_texts[0]):
		STATE.add_dialogue_thoughts.emit([
			UtilService.pop_random_stateful(quip_texts)
		])

	else:
		STATE.add_dialogue_thoughts.emit(['[wave]. . .'])


func _rotate(__target_angle: float) -> Tween:
	var time = min(abs(__target_angle - rotation.y), PI) * ROTATION_SPEED
	
	if abs(__target_angle - rotation.y) >= PI:
		__target_angle -= PI*2
	
	var tween = get_tree().create_tween()
	tween.tween_property(self, 'rotation:y', __target_angle, time)
	return tween


func _on_done_with_thoughts() -> void:
	if _is_in_dialogue:
		_rotate(_initial_rotation)
		_is_in_dialogue = false
