@tool
class_name InteractableItem extends Interactable


var _item_id
@export var item_id: ID.ITEM_TYPES = ID.ITEM_TYPES._NONE :
	set(__item_id):
		_item_id = __item_id
	get:
		if _item_id:
			return _item_id
		else:
			return ID.ITEM_TYPES._NONE


func _ready():
	super._ready()

	assert(_item_id, 'Item ID not set to a valid ID for %s!'%name)


func handle_interaction():
	STATE.add_inventory_item.emit(_item_id)

	# todo: effect (polish)
