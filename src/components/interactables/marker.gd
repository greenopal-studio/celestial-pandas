@tool
extends Node3D


var _color: Color = Color.DARK_ORANGE
@export var color: Color :
	set(__value):
		_color = __value
		if _sprite:
			_sprite.modulate = __value
	get:
		return _color

var _size: float = 1.0
@export var size: float :
	set(__value):
		_size = __value
		set_scale(Vector3(__value, __value, __value))
	get:
		return _size


@onready var _sprite: Sprite3D = $Sprite3D
@onready var _anim: AnimationPlayer = $AnimationPlayer


func _ready():
	_anim.play('default')
	
	_sprite.modulate = _color
	set_scale(Vector3(_size, _size, _size))
