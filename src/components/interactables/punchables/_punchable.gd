@tool
extends InteractableItem


@onready var _fruit_models_anchor: Node3D = $FruitModelsAnchor


func is_interactable() -> bool:
	return (
		STATE.get_has_learned_punch()
		and _fruit_models_anchor.visible
	)


func handle_interaction() -> void:
	super.handle_interaction()

	_fruit_models_anchor.hide()


func _on_end_day() -> void:
	_fruit_models_anchor.show()
