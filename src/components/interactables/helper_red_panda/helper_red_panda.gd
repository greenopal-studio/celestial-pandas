extends Interactable


const ROTATION_SPEED = 0.15
const PANDA_HIDDEN_POS = 0.2


@export_multiline var main_dialogue: Array[String] = ['']
@export var teaches_punch: bool = false
@export var teaches_claw: bool = false
@export_multiline var quip_texts: Array[String] = ['']


@onready var _initial_rotation: float = rotation.y
@onready var _reaction_shape := $Reaction/CollisionShape3D


var _is_in_dialogue := false
var _talk_phase := 1
var _visible := false
var _prev_text_id := -1


func _ready():
	super._ready()

	STATE.done_with_thoughts.connect(_on_done_with_thoughts)
	STATE.set_sacrifice_tries.connect(_on_set_sacrifice_tries)


func handle_interaction_with_player(__player_position: Vector3):
	_is_in_dialogue = true

	if _talk_phase > 1:
		var __direction = global_position.direction_to(__player_position)
		var __target_angle = __direction.signed_angle_to(Vector3.FORWARD, Vector3.UP) * -1.0
		_rotate(__target_angle)

	if _talk_phase == 1:
		STATE.add_dialogue_thoughts.emit([main_dialogue[0], main_dialogue[1]])
	
	elif _talk_phase == 2:
		STATE.add_dialogue_thoughts.emit([main_dialogue[2], main_dialogue[3]])
	
	elif _talk_phase == 3:
		var __constellation_state = STATE.get_constellation_state()
		var __found_constellations = 0
		for __constellation_id in __constellation_state:
			if __constellation_state[__constellation_id]:
				__found_constellations += 1
		var __next_text_id := STATE.rng.randi_range(
			0 + 3 * __found_constellations,
			2 + 3 * __found_constellations
		)
		while __next_text_id == _prev_text_id:
			__next_text_id = STATE.rng.randi_range(
				0 + 3 * __found_constellations,
				2 + 3 * __found_constellations
			)
		STATE.add_dialogue_thoughts.emit([quip_texts[__next_text_id]])
		_prev_text_id = __next_text_id
	elif _talk_phase == 4:
		STATE.add_dialogue_thoughts.emit([main_dialogue[4]])


func _rotate(__target_angle: float) -> Tween:
	var __time = min(abs(__target_angle - rotation.y), PI) * ROTATION_SPEED
	
	if abs(__target_angle - rotation.y) >= PI:
		__target_angle -= PI*2
	
	var __tween = get_tree().create_tween()
	__tween.tween_property(self, 'rotation:y', __target_angle, __time)
	return __tween


func _on_done_with_thoughts() -> void:
	if _is_in_dialogue and _talk_phase > 1:
		_rotate(_initial_rotation)
		_is_in_dialogue = false
		if _talk_phase == 4:
			_talk_phase -= 1
		elif _talk_phase < 3:
			_talk_phase += 1
	elif _is_in_dialogue and _talk_phase == 1:
		__move_panda(0.0)
		_reaction_shape.set_disabled(true)
		_talk_phase += 1
		_visible = true
		_is_in_dialogue = false


func __move_panda(__new_z: float, hide_after_end: bool = false) -> void:
	var __tween := get_tree().create_tween()
	__tween.tween_property($redpanda, 'position:z', __new_z, 0.5)
	if hide_after_end:
		await __tween.finished
		hide()


func _on_set_sacrifice_tries(__tries: int) -> void:
	if __tries == 0:
		if _visible:
			__move_panda(PANDA_HIDDEN_POS, true)
			_talk_phase = 4
			_visible = false
	elif __tries == 2:
		if _talk_phase == 1:
			show()
		elif _talk_phase == 4:
			show()
			__move_panda(0.0)
			_visible = true


func is_interactable() -> bool:
	return is_visible_in_tree()


func _on_end_day() -> void:
	pass
