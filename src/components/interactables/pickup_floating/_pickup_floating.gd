@tool
extends InteractableItem


@onready var _anim: AnimationPlayer  = %AnimationPlayer


func _ready():
	super._ready()

	_anim.play('default')


func _on_end_day() -> void:
	show()


func is_interactable() -> bool:
	return visible


func handle_interaction():
	super.handle_interaction()
	
	hide()
