@tool
extends InteractableItem


@onready var _ore_state: Node3D = %OreState


func is_interactable() -> bool:
	return (
		STATE.get_has_learned_claw()
		and _ore_state.visible
	)


func handle_interaction() -> void:
	super.handle_interaction()

	_ore_state.hide()


func _on_end_day() -> void:
	_ore_state.show()
