extends Sprite3D


@export var _item_type: ID.ITEM_TYPES = ID.ITEM_TYPES._NONE


func _ready() -> void:
	if _item_type != ID.ITEM_TYPES._NONE:
		$Rune.set_texture(DATA.items[_item_type].rune)
