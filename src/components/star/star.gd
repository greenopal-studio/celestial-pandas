extends Area2D


@export var _item_type: ID.ITEM_TYPES


@onready var _item_node: Sprite2D = $Item


var _shader: Dictionary = {
	'active': preload('res://assets/shaders/star_shader_mat.tres'),
	'inactive': preload('res://assets/shaders/star_inactive_shader_mat.tres'),
}

var _is_highlighted: bool = false
var _is_active := true


func _ready() -> void:
	_item_node.set_texture(DATA.items[_item_type].icon)
	await get_tree().create_timer(0.5 * randf()).timeout
	$AnimatedSprite2D.play('default')


func highlight() -> void:
	_is_highlighted = true
	queue_redraw()


func unhighlight() -> void:
	_is_highlighted = false
	queue_redraw()


func activate() -> void:
	modulate.a = 1.0
	_item_node.set_material(_shader.active)
	_is_active = true


func deactivate() -> void:
	modulate.a = 0.4
	_item_node.set_material(_shader.inactive)
	_is_active = false


func get_item_type() -> ID.ITEM_TYPES:
	return _item_type


func is_active() -> bool:
	return _is_active


func _draw() -> void:
	if _is_highlighted:
		draw_arc(Vector2.ZERO, 23.0, 0.0, PI * 2.0, 30, Color.DARK_CYAN, 2.0)
