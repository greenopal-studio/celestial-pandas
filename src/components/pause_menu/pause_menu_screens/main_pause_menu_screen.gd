class_name MainPauseMenuScreen extends CenterContainer


signal opened_save_screen()
signal exited()


@onready var _save_button: Button = %SaveButton
@onready var _exit_button: Button = %ExitButton


func _ready():
	visibility_changed.connect(_on_visibility_changed)

	_save_button.pressed.connect(_on_save_pressed)
	_exit_button.pressed.connect(_on_exit_pressed)


func _on_visibility_changed():
	if visible and INPUTSTATE.get_is_input_via_joypad():
		_save_button.grab_focus.call_deferred()


func _on_save_pressed() -> void:
	opened_save_screen.emit()


func _on_exit_pressed() -> void:
	exited.emit()
