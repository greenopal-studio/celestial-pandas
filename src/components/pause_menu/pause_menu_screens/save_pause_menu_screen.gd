extends CenterContainer


signal saved()
signal canceled()


@onready var _save_name_text_edit: TextEdit = %NameTextEdit
@onready var _confirm_button: Button = %ConfirmButton
@onready var _cancel_button: Button = %CancelButton


func _ready():
	visibility_changed.connect(_on_visibility_changed)

	_confirm_button.pressed.connect(_on_confirm_pressed)
	_cancel_button.pressed.connect(_on_cancel_pressed)


func _on_visibility_changed() -> void:
	if visible:
		if STATE.get_current_save_id():
			_save_name_text_edit.editable = false
			var save_meta = STATE.get_current_save_meta()
			_save_name_text_edit.text = save_meta.get('name', '')
		else:
			_save_name_text_edit.editable = true
			_save_name_text_edit.text = ''


func _on_confirm_pressed():
	STATE.save_data({
		'name': _save_name_text_edit.text
	})

	saved.emit()


func _on_cancel_pressed():
	canceled.emit()
