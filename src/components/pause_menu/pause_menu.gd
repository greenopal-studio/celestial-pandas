extends PanelContainer


@onready var _main_menu_screen: MainPauseMenuScreen = %MainPauseMenuScreen
@onready var _save_menu_screen = %SavePauseMenuScreen


var _previous_mouse_mode


func _ready():
	hide()

	_main_menu_screen.opened_save_screen.connect(_on_main_opened_save_screen)
	_main_menu_screen.exited.connect(_on_main_exited)
	_save_menu_screen.saved.connect(_on_save_saved)
	_save_menu_screen.canceled.connect(_on_save_canceled)


func _unhandled_input(_event) -> void:
	if Input.is_action_just_pressed('pause_menu'):
		if visible:
			get_tree().paused = false
			hide()
			Input.set_mouse_mode(_previous_mouse_mode)
		else:
			get_tree().paused = true
			_main_menu_screen.show()
			_save_menu_screen.hide()
			show()
			_previous_mouse_mode = Input.get_mouse_mode()
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_main_opened_save_screen() -> void:
	_main_menu_screen.hide()
	_save_menu_screen.show()


func _on_main_exited() -> void:
	get_tree().paused = false
	SCENE.goto_scene(ID.SCENE_TYPES.MENU)


func _on_save_saved() -> void:
	hide()


func _on_save_canceled() -> void:
	_save_menu_screen.hide()
	_main_menu_screen.show()
