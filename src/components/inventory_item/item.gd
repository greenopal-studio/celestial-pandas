extends PanelContainer


signal hovered(__item_id: ID.ITEM_TYPES)
signal selected(__item_id: ID.ITEM_TYPES)


@onready var _icon_node := $Icon
@onready var _selected_node := $Selected


var _styles: Dictionary = {
	'normal': 'ItemPanel',
	'hovered': 'ItemHoveredPanel',
}
var _is_selected := false
var _item_type: ID.ITEM_TYPES = ID.ITEM_TYPES._NONE


func _ready() -> void:
	mouse_entered.connect(_on_mouse_entered)
	mouse_exited.connect(_on_mouse_exited)
	gui_input.connect(_on_gui_input)


func _on_mouse_entered() -> void:
	set_theme_type_variation(_styles.hovered)
	emit_signal('hovered', _item_type)


func _on_mouse_exited() -> void:
	set_theme_type_variation(_styles.normal)
	emit_signal('hovered', ID.ITEM_TYPES._NONE)


func set_item(__item_id: ID.ITEM_TYPES) -> void:
	_item_type = __item_id
	if __item_id == ID.ITEM_TYPES._NONE:
		_icon_node.set_texture(null)
	else:
		_icon_node.set_texture(DATA.items[__item_id].icon)


func get_item_type() -> ID.ITEM_TYPES:
	return _item_type


func _on_gui_input(__event: InputEvent) -> void:
	if __event is InputEventMouseButton and Input.is_action_just_pressed('inventory_select_item'):
		if _is_selected:
			_is_selected = false
			#_selected_node.hide()
		else:
			_is_selected = true
			#_selected_node.show()
		emit_signal('selected', _item_type)
