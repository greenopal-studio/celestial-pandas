class_name ThemeService


const min_font_size = 8
const max_font_size = 42


static func get_current_theme() -> Theme:
	var root_node = ThemeService._get_root_node()
	if not root_node:
		return null
	if root_node.theme:
		return root_node.theme

	return ThemeDB.get_project_theme().duplicate(true)


static func set_font_scale(__scale: float) -> void:
	var root_node = ThemeService._get_root_node()
	if not root_node:
		return

	var source_theme = ThemeDB.get_project_theme()
	var used_theme = ThemeService.get_current_theme()

	var source_default_font_size = source_theme.get_default_font_size()
	var target_default_font_size = source_default_font_size * __scale
	if target_default_font_size < min_font_size:
		target_default_font_size = min_font_size
		__scale = float(min_font_size) / float(source_default_font_size)
	elif target_default_font_size > max_font_size:
		target_default_font_size = max_font_size
		__scale = float(max_font_size) / float(source_default_font_size)
	used_theme.set_default_font_size(target_default_font_size)

	for font_size_type in used_theme.get_font_size_type_list():
		for font_size_name in used_theme.get_font_size_list(font_size_type):
			var source_font_size = source_theme.get_font_size(font_size_name, font_size_type)
			var target_font_size = source_font_size * __scale
			used_theme.set_font_size(font_size_name, font_size_type, target_font_size)

	root_node.theme = used_theme
	root_node.propagate_call("update")


static func _get_root_node() -> Window:
	var scene_tree =  Engine.get_main_loop() as SceneTree
	if not scene_tree:
		return null
	return scene_tree.get_root()
