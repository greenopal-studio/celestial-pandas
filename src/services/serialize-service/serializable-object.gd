class_name SerializableObject extends RefCounted # okay I lied, it's not an object
# But RefCounted is just a better Object, so shh...


static func get_type_hints() -> Dictionary:
	return {}


@warning_ignore('UNUSED_PARAMETER') # will be over-written be extending class
static func migrate(old_version: int, data: Dictionary) -> void:
	return
