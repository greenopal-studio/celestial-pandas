class_name ResourceService


static func load_resource_folder_as_dict(__path: String) -> Dictionary:
	var result = {}

	for file_path in _get_files_in_dir(__path):
		var resource = load(__path + '/' + file_path)
		if not resource:
			_get_logger().error('Could not load resource at path "%s"'%__path)
			continue
		if not 'id' in resource or resource['id'] == 0:
			_get_logger().error('Invalid resource at path "%s", does not have an id set'%__path)
			continue
		result[resource['id']] = resource
	
	_get_logger().info('Successfully loaded %d resources from path "%s": %s'%[
		result.size(), __path, result.keys()
	])

	return result


static func _get_files_in_dir(__path: String) -> PackedStringArray:
	if not __path.contains('://'):
		__path = 'res://' + __path

	if not DirAccess.dir_exists_absolute(__path):
		return PackedStringArray([])
		
	return DirAccess.get_files_at(__path)


static func _get_logger() -> Logger:
	return LOGGING.get_logger('ResourceService')
