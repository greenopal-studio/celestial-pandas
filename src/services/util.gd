class_name UtilService


static func is_debug() -> bool:
	return OS.has_feature('editor')


static func format_count(__count: int) -> String:
	var text = str(__count)

	if __count >= 1_000_000_000:
		text = 'a lot'
	elif __count >= 1_000_000:
		@warning_ignore('INTEGER_DIVISION') # it is intended that we discard the decimal part...
		text = '%s.%03d.%03d' % [
			floor(__count / 1_000_000), floor((__count % 1_000_000) / 1_000), __count % 1_000
		]
	elif __count >= 1_000:
		@warning_ignore('INTEGER_DIVISION') # ...same here...
		text = '%s.%03d' % [floor(__count / 1_000), __count % 1_000]

	return text


static func format_time_to_minutes(__seconds: int) -> String:
	@warning_ignore('INTEGER_DIVISION') # ...same here...
	return '%02d:%02d' % [floor(__seconds / 60), __seconds % 60]


static func is_pos_inside_rect(__pos: Vector2, __rect_top_left: Vector2, __rect_bottom_right: Vector2) -> bool:
	return (
		__pos.x >= __rect_top_left.x and __pos.x <= __rect_bottom_right.x
		and __pos.y >= __rect_top_left.y and __pos.y <= __rect_bottom_right.y
	)


static func pick_random_stateful(__arr: Array[Variant]) -> Variant:
	if len(__arr) < 1:
		return null

	if len(__arr) == 1:
		return __arr[0]

	return __arr[
		STATE.rng.randi_range(0, len(__arr) - 1)
	]


static func pop_random_stateful(__arr: Array[Variant]) -> Variant:
	if len(__arr) < 1:
		return null

	if len(__arr) == 1:
		return __arr.pop_front()

	return __arr.pop_at(
		STATE.rng.randi_range(0, len(__arr) - 1)
	)
