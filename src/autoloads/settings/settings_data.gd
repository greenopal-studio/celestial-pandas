class_name SettingsData extends SerializableObject

var _volume: float
var _locale: String
var _camera_invert_x: bool
var _camera_invert_y: bool
var _camera_joy_stick_mode: int
var _font_scale: float


func _init(
	__volume: float = 0.0,
	__locale: String = OS.get_locale(),
	__camera_invert_x: bool = false,
	__camera_invert_y: bool = false,
	__camera_joy_stick_mode: int = 0,
	__font_scale: float = 1.0
):
	_volume = __volume
	_locale = __locale
	_camera_invert_x = __camera_invert_x
	_camera_invert_y = __camera_invert_y
	_camera_joy_stick_mode = __camera_joy_stick_mode
	_font_scale = __font_scale


static func migrate(__old_version: int, __data: Dictionary) -> void:
	if __old_version == 0:
		if __data.has('_invert_camera_x'):
			__data['_camera_invert_x'] = __data['_invert_camera_x']
			__data.erase('_invert_camera_x')
		if __data.has('_invert_camera_y'):
			__data['_camera_invert_y'] = __data['_invert_camera_y']
			__data.erase('_invert_camera_y')


func duplicate() -> SettingsData:
	return SettingsData.new(
		_volume,
		_locale,
		_camera_invert_x,
		_camera_invert_y,
		_camera_joy_stick_mode,
		_font_scale
	)


func set_volume(__new_volume: float) -> bool:
	if _volume == __new_volume:
		return false
	_volume = __new_volume
	return true


func get_volume() -> float:
	return _volume


func set_locale(__new_locale: String) -> bool:
	if _locale == __new_locale:
		return false
	_locale = __new_locale
	return true


func get_locale() -> String:
	return _locale


func set_camera_invert_x(__new_value: bool) -> bool:
	if _camera_invert_x == __new_value:
		return false
	_camera_invert_x = __new_value
	return true


func get_camera_invert_x() -> bool:
	return _camera_invert_x


func set_camera_invert_y(__new_value: bool) -> bool:
	if _camera_invert_y == __new_value:
		return false
	_camera_invert_y = __new_value
	return true


func get_camera_invert_y() -> bool:
	return _camera_invert_y


func set_camera_joy_stick_mode(__new_value: int) -> bool:
	if _camera_joy_stick_mode == __new_value:
		return false
	_camera_joy_stick_mode = __new_value
	return true


func get_camera_joy_stick_mode() -> int:
	return _camera_joy_stick_mode


func set_font_scale(__new_value: float) -> bool:
	if _font_scale == __new_value:
		return false
	_font_scale = __new_value
	return true


func get_font_scale() -> float:
	return _font_scale
