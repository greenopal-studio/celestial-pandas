extends Node


const _SAVE_FILE_NAME = 'user://settings.save'


var _data: SettingsData


func _init():
	if FileAccess.file_exists(_SAVE_FILE_NAME):
		_data = SerializeService.load_data(_SAVE_FILE_NAME, SettingsData)
		set_data(_data, ID.SETTING_TYPES.values())
	else:
		_data = SettingsData.new()
		_save_data()


func _ready():
	ThemeService.set_font_scale(_data.get_font_scale())


func get_data(__duplicate: bool = false) -> SettingsData:
	if __duplicate:
		return _data.duplicate()
	return _data


func set_data(__data: SettingsData, __changed_settings: Array = []):
	for __changed_setting in __changed_settings:
		match __changed_setting:
			ID.SETTING_TYPES.VOLUME:
				_data.set_volume(__data.get_volume())
				AudioServer.set_bus_volume_db(AudioServer.get_bus_index('Master'), __data.get_volume())
			ID.SETTING_TYPES.LOCALE:
				_data.set_locale(__data.get_locale())
				TranslationServer.set_locale(__data.get_locale())
			ID.SETTING_TYPES.CAMERA_INVERT_X:
				_data.set_camera_invert_x(__data.get_camera_invert_x())
			ID.SETTING_TYPES.CAMERA_INVERT_Y:
				_data.set_camera_invert_y(__data.get_camera_invert_y())
			ID.SETTING_TYPES.CAMERA_JOY_STICK_MODE:
				_data.set_camera_joy_stick_mode(__data.get_camera_joy_stick_mode())
			ID.SETTING_TYPES.FONT_SCALE:
				_data.set_font_scale(__data.get_font_scale())
				ThemeService.set_font_scale(__data.get_font_scale())
	_save_data()


func _save_data():
	SerializeService.save_data_with_full_path(_SAVE_FILE_NAME, 0, {}, _data)
