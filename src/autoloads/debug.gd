extends Node

var view_scene = preload('res://components/_debug/debug_view.tscn')


var view = null


func _ready():
	if UtilService.is_debug():
		view = view_scene.instantiate()
		add_child(view)


func add_console_cmd(__cmd: String, __f: Callable) -> void:
	if UtilService.is_debug() and view:
		view.add_console_cmd(__cmd, __f)

	
func add_console_log_msg(__text: String, __color: Color = Color.WHITE) -> void:
	if UtilService.is_debug() and view:
		view.add_console_log_msg(__text, __color)


func add_action_button(__text: String, __f: Callable) -> void:
	if UtilService.is_debug() and view:
		view.add_action_button(__text, __f)


func add_value_header(__label_text: String, __show_in_overlay: bool = false) -> void:
	if UtilService.is_debug() and view:
		view.add_value_header(__label_text, __show_in_overlay)


func add_value(
	__label_text: String, __obj: Variant, __expression_str: String, __show_in_overlay: bool = false
) -> void:
	if UtilService.is_debug() and view:
		view.add_value(__label_text, __obj, __expression_str, __show_in_overlay)


func one_shot_value(__text: String) -> void:
	if UtilService.is_debug() and view:
		view.one_shot_value(__text)
