class_name Constellation extends Resource


@export var id : CONSTELLATION_TYPES
@export var needed_items: Array[ID.ITEM_TYPES]
@export var needed_connections: Array[Array]
@export var island_unlocked: ID.ISLAND_TYPES


enum CONSTELLATION_TYPES {
	_NONE,
	CONSTELLATION_DESERT,
	CONSTELLATION_JUICY,
	CONSTELLATION_SHINY,
	CONSTELLATION_FINAL,
}
