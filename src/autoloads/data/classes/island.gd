class_name Island extends Resource


@export var id : ISLAND_TYPES
@export var label : String
@export var has_items: Array[ID.ITEM_TYPES]


enum ISLAND_TYPES {
	_NONE,
	CAVE_ISLAND,
	NEBULA_ISLAND,
	DESERT_ISLAND,
	JUICY_ISLAND,
	SHINY_ISLAND,
	FINAL_ISLAND,
}
