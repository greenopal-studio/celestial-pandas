class_name Item extends Resource


@export var id : ITEM_TYPES
@export var label : String
@export var found_on : Array[ID.ISLAND_TYPES]
@export var aquired_through : Array[AQUIRING_TYPES]
@export var purposes : Array[PURPOSE_TYPES]
@export var icon : Texture2D
@export var rune : Texture2D
@export var rune_emission : Texture2D
@export var obj_scene : PackedScene


enum ITEM_TYPES {
	_NONE,
	STAR_FRUIT,
	ROCK,
	MOON_FRUIT,
	MOSS,
	NEBULA_LEAF,
	SPACE_CACTI,
	APPLE,
	PEACH,
	KWARDIUM_ORE,
	OPAL_STONE,
}


enum AQUIRING_TYPES {
	PICK_UP,
	PUNCH_CACTUS,
	PUNCH_TREE,
	CLAW_MOUNTAIN,
}


enum PURPOSE_TYPES {
	ACTIVATE_STAR_VIEW,
	CONSTELLATION,
}


const AQUIRING_LABELS = {
	AQUIRING_TYPES.PICK_UP: 'Picking up',
	AQUIRING_TYPES.PUNCH_CACTUS: 'Punch the cactus',
	AQUIRING_TYPES.PUNCH_TREE: 'Punch the correct tree',
	AQUIRING_TYPES.CLAW_MOUNTAIN: 'Claw at the mountain',
}


const PURPOSE_LABELS = {
	PURPOSE_TYPES.ACTIVATE_STAR_VIEW: 'Used for activating the star view',
	PURPOSE_TYPES.CONSTELLATION: 'Used for building a constellation',
}
