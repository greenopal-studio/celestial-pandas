class_name State extends Node


# INTERNAL STATE DATA:

var rng := RandomNumberGenerator.new()
var _logger: Logger = LOGGING.get_logger('STATE')
var _inventory_items: Array[ID.ITEM_TYPES] = []
var _constellation_state: Dictionary = {
	ID.CONSTELLATION_TYPES.CONSTELLATION_DESERT: false,
	ID.CONSTELLATION_TYPES.CONSTELLATION_JUICY: false,
	ID.CONSTELLATION_TYPES.CONSTELLATION_SHINY: false,
	ID.CONSTELLATION_TYPES.CONSTELLATION_FINAL: false,
}
var _day: int = 1
var _sacrifice_tries := 0
var _has_learned_punch := false
var _has_learned_claw := false


static func get_type_hints() -> Dictionary:
	return {
	}


func reset() -> void:
	rng.randomize()

	__current_save_id = ''

	_constellation_state = {
		ID.CONSTELLATION_TYPES.CONSTELLATION_DESERT: false,
		ID.CONSTELLATION_TYPES.CONSTELLATION_JUICY: false,
		ID.CONSTELLATION_TYPES.CONSTELLATION_SHINY: false,
		ID.CONSTELLATION_TYPES.CONSTELLATION_FINAL: false,
	}
	_inventory_items = []

	_day = 1

	_sacrifice_tries = 0

	_has_learned_punch = false
	_has_learned_claw = false


@warning_ignore('UNUSED_PARAMETER') # todo: placeholder
static func migrate(__old_version: int, __data: Dictionary) -> void:
	pass


# STATE DATA GETTERS:

func get_inventory_items() -> Array[ID.ITEM_TYPES]:
	return _inventory_items


func get_constellation_state() -> Dictionary:
	return _constellation_state


func get_day() -> int:
	return _day


func get_sacrifice_tries() -> int:
	return _sacrifice_tries


func get_has_learned_punch() -> bool:
	return _has_learned_punch


func get_has_learned_claw() -> bool:
	return _has_learned_claw


# SIGNALS AND HANDLERS:

signal add_inventory_item(inventory_item: ID.ITEM_TYPES)

func _on_add_inventory_item(__new_item_id: ID.ITEM_TYPES) -> void:
	if not __new_item_id in _inventory_items:
		_inventory_items.append(__new_item_id)


signal remove_inventory_item(inventory_item: ID.ITEM_TYPES)

func _on_remove_inventory_item(__item_id: ID.ITEM_TYPES) -> void:
	_inventory_items.erase(__item_id)


signal set_sacrifice_tries(tries: int)

func _on_set_sacrifice_tries(__new_tries: int) -> void:
	_sacrifice_tries = __new_tries


signal add_hint_thoughts(thoughts: Array[String])


signal add_dialogue_thoughts(thoughts: Array[String])


signal clear_thoughts()


signal done_with_thoughts() # just vibes now


signal start_star_view()

signal leave_star_view(confirmed: bool, constellation_id: ID.CONSTELLATION_TYPES, items_used: Array[ID.ITEM_TYPES])

signal set_gui_visible(is_visible: bool)

signal end_day()

func _on_end_day() -> void:
	_day += 1


signal constellation_summoned(constellation: ID.CONSTELLATION_TYPES)

func _on_constellation_summoned(__constellation: ID.CONSTELLATION_TYPES) -> void:
	_constellation_state[__constellation] = true


signal learn_punch()

func _on_learn_punch() -> void:
	_has_learned_punch = true


signal learn_claw()

func _on_learn_claw() -> void:
	_has_learned_claw = true


# SAVESTATE METHODS:

const _SUB_PATH = 'savestate'
const _CURRENT_VERSION = 0

var __current_save_id: String = ''
var __current_save_meta: Dictionary = {}


func get_current_save_id() -> String:
	return __current_save_id


func get_current_save_meta() -> Dictionary:
	return __current_save_meta


func get_savestate_files() -> PackedStringArray:
	if DirAccess.dir_exists_absolute('user://' + _SUB_PATH):
		return DirAccess.get_files_at('user://' + _SUB_PATH)
	return []


func save_data(__meta: Dictionary = {}) -> void:
	if not __current_save_id:
		__current_save_id = SerializeService.save_data_with_generated_id(
			_SUB_PATH, _CURRENT_VERSION, __meta, self
		)
		__current_save_meta = __meta
	else:
		SerializeService.save_data_with_id(
			_SUB_PATH, __current_save_id, _CURRENT_VERSION, __meta, self
		)
		__current_save_meta = __meta


func load_meta(__save_id: String) -> Variant:
	return SerializeService.load_meta_with_id(_SUB_PATH, __save_id)


func load_data(__save_id: String) -> bool:
	var success = SerializeService.load_data_with_id_into_object(_SUB_PATH, __save_id, State, self)
	if success:
		__current_save_id = __save_id
		__current_save_meta = load_meta(__save_id)
	return success


# HELPER METHODS:

# vararg is not supported in GDscript (yet) :/

func _log_signal_0(__signal_name: String) -> void:
	_logger.info('got signal %s'%[
		__signal_name
	])

func _log_signal_1(__arg1, __signal_name: String) -> void:
	_logger.info('got signal %s with argument %s'%[
		__signal_name, __arg1
	])

func _log_signal_2(__arg1, __arg2, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s'%[
		__signal_name, __arg1, __arg2
	])

func _log_signal_3(__arg1, __arg2, __arg3, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3
	])

func _log_signal_4(__arg1, __arg2, __arg3, __arg4, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3, __arg4
	])

func _log_signal_5(__arg1, __arg2, __arg3, __arg4, __arg5, __signal_name: String) -> void:
	_logger.info('got signal %s with arguments %s , %s , %s , %s , %s'%[
		__signal_name, __arg1, __arg2, __arg3, __arg4, __arg5
	])


func _ready():	
	# Automatically connect _on_ handler methods to signals:
	for method_dict in get_method_list():
		var method_name = method_dict['name']
		if not method_name.begins_with('_on_'):
			continue

		var signal_name = method_name.substr(4) # len('_on_') = 4
		if !has_signal(signal_name):
			_logger.error('No matching signal defined for handler method %s! You should add a %s signal.'%[
				method_name, signal_name
			])
			continue

		connect(signal_name, Callable(self, method_name))

	for signal_dict in get_signal_list():
		match len(signal_dict.args):
			0:
				connect(signal_dict['name'], Callable(self, '_log_signal_0').bind(signal_dict['name']))
			1:
				connect(signal_dict['name'], Callable(self, '_log_signal_1').bind(signal_dict['name']))
			2:
				connect(signal_dict['name'], Callable(self, '_log_signal_2').bind(signal_dict['name']))
			3:
				connect(signal_dict['name'], Callable(self, '_log_signal_3').bind(signal_dict['name']))
			4:
				connect(signal_dict['name'], Callable(self, '_log_signal_4').bind(signal_dict['name']))
			5:
				connect(signal_dict['name'], Callable(self, '_log_signal_5').bind(signal_dict['name']))
			_:
				_logger.warning('Too many args for signal %s, please add an appropriate _log_signal_%d func.'%[
					signal_dict['name'], len(signal_dict.args)
				])
				connect(signal_dict['name'], Callable(self, '_log_signal_0').bind(signal_dict['name']))
 
