extends Node


func _ready() -> void:
	var __audio_node = AudioStreamPlayer.new()
	__audio_node.set_stream(load('res://assets/music/menu_music.mp3'))
	add_child(__audio_node)
	__audio_node.finished.connect(_on_audio_finished.bind(__audio_node))
	__audio_node.play()


func _on_audio_finished(__audio_node: AudioStreamPlayer) -> void:
	__audio_node.play()


func stop_menu_music() -> void:
	get_child(0).stop()


func play_menu_music() -> void:
	if not get_child(0).is_playing():
		get_child(0).play()
