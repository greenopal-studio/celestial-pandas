extends VBoxContainer


func _ready():
	$'StartButton'.pressed.connect(_on_start_pressed)
	$'QuitButton'.pressed.connect(_on_quit_pressed)


func _on_start_pressed() -> void:
	STATE.reset()
	SCENE.goto_scene(ID.SCENE_TYPES.WORLD)


func _on_quit_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.MENU)
