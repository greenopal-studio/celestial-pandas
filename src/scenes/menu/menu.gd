extends VBoxContainer


func _ready():
	$'StartButton'.pressed.connect(_on_start_pressed)
	$'SettingsButton'.pressed.connect(_on_settings_pressed)
	$'QuitButton'.pressed.connect(_on_quit_pressed)

	if INPUTSTATE.get_is_input_via_joypad():
		$'OfficeButton'.grab_focus.call_deferred()
	
	MUSIC.play_menu_music()


func _on_start_pressed() -> void:
	MUSIC.stop_menu_music()
	STATE.reset()
	SCENE.goto_scene(ID.SCENE_TYPES.WORLD)


func _on_settings_pressed() -> void:
	SCENE.goto_scene(ID.SCENE_TYPES.SETTING)


func _on_quit_pressed() -> void:
	get_tree().quit()
