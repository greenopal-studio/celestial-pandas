extends Control


@onready var _animation_player: AnimationPlayer = $AnimationPlayer
@onready var _content_node: VBoxContainer = $'%Content'
@onready var _item_icon_node: TextureRect = $'%ItemIcon'
@onready var _item_name_node: Label = $'%ItemName'
@onready var _found_on_list_node: VBoxContainer = $'%Places'
@onready var _aquired_through_list_node: VBoxContainer = $'%AquiringTypes'
@onready var _purposes_list_node: VBoxContainer = $'%Purposes'
@onready var _item_slots_node: GridContainer = $'%ItemSlots'


var _hovered_item: ID.ITEM_TYPES = ID.ITEM_TYPES._NONE
var _selected_items: Array[ID.ITEM_TYPES] = []


func _ready() -> void:
	STATE.add_inventory_item.connect(add_item)
	STATE.remove_inventory_item.connect(remove_item)
	for __item_slot in _item_slots_node.get_children():
		__item_slot.hovered.connect(_on_item_hovered)
		__item_slot.selected.connect(_on_item_selected)


func activate() -> void:
	_animation_player.play('enter')


func deactivate() -> void:
	_animation_player.play('leave')


func _on_item_hovered(__item_id: ID.ITEM_TYPES) -> void:
	if __item_id == ID.ITEM_TYPES._NONE:
		_content_node.hide()
		return
	
	_item_icon_node.set_texture(DATA.items[__item_id].icon)
	_item_name_node.set_text(DATA.items[__item_id].label)
	
	for __child in _found_on_list_node.get_children():
		__child.queue_free()
	for __found_on in DATA.items[__item_id].found_on:
		_found_on_list_node.add_child(
			__create_new_list_label(
				DATA.islands[__found_on].label
			)
		)
	
	for __child in _aquired_through_list_node.get_children():
		__child.queue_free()
	for __aquired_through in DATA.items[__item_id].aquired_through:
		_aquired_through_list_node.add_child(
			__create_new_list_label(
				Item.AQUIRING_LABELS[__aquired_through]
			)
		)
	
	for __child in _purposes_list_node.get_children():
		__child.queue_free()
	for __purpose in DATA.items[__item_id].purposes:
		_purposes_list_node.add_child(
			__create_new_list_label(
				Item.PURPOSE_LABELS[__purpose]
			)
		)
	
	if not _content_node.is_visible_in_tree():
		_content_node.show()
	
	_hovered_item = __item_id


func _on_item_selected(__item_id: ID.ITEM_TYPES) -> void:
	if __item_id in _selected_items:
		_selected_items.erase(__item_id)
	else:
		_selected_items.append(__item_id)


func add_item(__new_item_id: ID.ITEM_TYPES) -> void:
	var __item_already_here := false
	for __child in _item_slots_node.get_children():
		if __child.get_item_type() == __new_item_id:
			__item_already_here = true
	if not __item_already_here:
		_item_slots_node.get_child(STATE.get_inventory_items().size() - 1).set_item(__new_item_id)


func remove_item(__item_id: ID.ITEM_TYPES) -> void:
	var __items = STATE.get_inventory_items()
	for __i in _item_slots_node.get_child_count():
		var __refresh_item_id := ID.ITEM_TYPES._NONE
		if __items.size() > __i:
			__refresh_item_id = __items[__i]
		_item_slots_node.get_child(__i).set_item(__refresh_item_id)


func __create_new_list_label(__text: String) -> Label:
	var __new_label = Label.new()
	__new_label.set_text('- ' + __text)
	__new_label.set_theme_type_variation('HeaderSmall')
	return __new_label
