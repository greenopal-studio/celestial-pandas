extends Node2D


const _LINE_COLOR = Color.GHOST_WHITE
const _LINE_SELECTED_COLOR = Color.CRIMSON
const _OLD_LINE_COLOR = Color(0.24, 0.24, 0.24)
const _LINE_WIDTH = 6.0


@onready var _stars_node: Node2D = $Stars
@onready var _connections_node: Node2D = $Connections
@onready var _old_connections_node: Node2D = $OldConnections
@onready var _leave_button: Button = %LeaveButton
@onready var _confirm_button: Button = %ConfirmButton


var _connections: Array[Array] = []
var _hovered_star: Area2D = null
var _hovered_connection: Line2D = null
var _line: Dictionary = {
	'is_drawing': false,
	'start': Vector2.ZERO,
	'start_node': null,
	'end': Vector2.ZERO
}


func _ready() -> void:
	for __star_node: Area2D in _stars_node.get_children():
		# these somehow don't work in a subviewport
		__star_node.mouse_entered.connect(_on_star_entered.bind(__star_node))
		__star_node.mouse_exited.connect(_on_star_exited.bind(__star_node))
		__star_node.input_event.connect(_on_star_input_event.bind(__star_node))
	
	STATE.start_star_view.connect(_on_start_star_view)
	STATE.leave_star_view.connect(_on_leave_star_view)
	STATE.constellation_summoned.connect(_on_constellation_summoned)
	
	_leave_button.pressed.connect(_on_leave_pressed)
	_confirm_button.pressed.connect(_on_confirm_pressed)


func _on_leave_star_view(__confirmed: bool, __constellation_id: ID.CONSTELLATION_TYPES, __items: Array) -> void:
	for __child in _connections_node.get_children():
		__child.queue_free()
	_connections = []
	_leave_button.set_disabled(true)
	_confirm_button.set_disabled(true)


func _on_start_star_view() -> void:
	_leave_button.set_disabled(false)
	__check_items()


func _on_constellation_summoned(__constellation_id: ID.CONSTELLATION_TYPES) -> void:
	var __island_type = DATA.constellations[__constellation_id].island_unlocked
	var __newly_available_items = DATA.islands[__island_type].has_items
	for __item_id in __newly_available_items:
		for __star_node: Area2D in _stars_node.get_children():
			if __star_node.get_item_type() == __item_id:
				__star_node.show()


func _on_leave_pressed() -> void:
	STATE.emit_signal('leave_star_view', false, ID.CONSTELLATION_TYPES._NONE, [])


func _on_confirm_pressed() -> void:
	var __found_constellation := ID.CONSTELLATION_TYPES._NONE
	var __constellation_state = STATE.get_constellation_state()
	for __constellation_id in __constellation_state:
		if not __constellation_state[__constellation_id]:
			var __constellation_data = DATA.constellations[__constellation_id]
			var __amount_connections_found := 0
			for __connection in __constellation_data.needed_connections:
				var __connection_found := false
				for __line in _connections:
					if (__line[0].get_item_type() in __connection and 
						__line[1].get_item_type() in __connection):
						__connection_found = true
						break
				if __connection_found:
					__amount_connections_found += 1
			if (__amount_connections_found == __constellation_data.needed_connections.size() and
				__amount_connections_found == _connections.size()):
				__found_constellation = __constellation_id
				break
	if __found_constellation != ID.CONSTELLATION_TYPES._NONE:
		for __line: Line2D in _connections_node.get_children():
			_connections_node.remove_child(__line)
			_old_connections_node.add_child(__line)
			__line.set_default_color(_OLD_LINE_COLOR)
		for __item_id in DATA.constellations[__found_constellation].needed_items:
			STATE.emit_signal('remove_inventory_item', __item_id)
		STATE.emit_signal(
			'leave_star_view',
			true,
			__found_constellation,
			DATA.constellations[__found_constellation].needed_items
		)
	else:
		var __items_wasted = []
		for __line in _connections:
			if not __line[0].get_item_type() in __items_wasted:
				__items_wasted.append(__line[0].get_item_type())
			if not __line[1].get_item_type() in __items_wasted:
				__items_wasted.append(__line[1].get_item_type())
		for __item_id in __items_wasted:
			STATE.emit_signal('remove_inventory_item', __item_id)
		STATE.emit_signal('leave_star_view', true, ID.CONSTELLATION_TYPES._NONE, __items_wasted)


func __check_items() -> void:
	for __star_node: Area2D in _stars_node.get_children():
		if not __star_node.get_item_type() in STATE.get_inventory_items():
			__star_node.deactivate()
		else:
			__star_node.activate()


func _input(__event: InputEvent) -> void:
	if __event is InputEventMouseMotion:
		var __found_star := false
		for __star_node: Area2D in _stars_node.get_children():
			if __event.get_position().distance_squared_to(__star_node.get_position()) <= 2116.0: # 46^2
				if _hovered_star != null:
					_hovered_star.unhighlight()
				_on_star_entered(__star_node)
				__found_star = true
		if not __found_star and _hovered_star != null:
			_on_star_exited(_hovered_star)
		
		if _line.is_drawing:
			_line.end = __event.get_position()
			queue_redraw()
		elif not _line.is_drawing:
			var __found_connection := false
			for __connection: Line2D in _connections_node.get_children():
				var __ap: Vector2 = __event.get_position() - __connection.get_point_position(0)
				var __ab: Vector2 = __connection.get_point_position(1) - __connection.get_point_position(0)
				var __atob: float = pow(__ab.x, 2.0) + pow(__ab.y, 2.0)
				var __apab: float = __ap.x * __ab.x + __ap.y * __ab.y
				var __t: float = __apab / __atob
				if __t <= 0.0 or __t >= 1.0:
					continue
				var __closest_point: Vector2 = __connection.get_point_position(0) + __ab * __t
				if __event.get_position().distance_squared_to(__closest_point) <= 1024.0:
					if _hovered_connection != __connection:
						if _hovered_connection != null:
							_hovered_connection.set_default_color(_LINE_COLOR)
						__connection.set_default_color(_LINE_SELECTED_COLOR)
						_hovered_connection = __connection
					__found_connection = true
					break
			if not __found_connection and _hovered_connection != null:
				_hovered_connection.set_default_color(_LINE_COLOR)
				_hovered_connection = null
	elif _hovered_star != null:
		_on_star_input_event(null, __event, 0, _hovered_star)
	elif Input.is_action_just_released('click_star') and (_hovered_star == null or
		_hovered_star == _line.start_node):
		__reset_line()
	if Input.is_action_just_pressed('click_star_line') and _hovered_connection != null:
		for __entry in _connections:
			if _hovered_connection in __entry:
				_connections.erase(__entry)
				break
		_connections_node.remove_child(_hovered_connection)
		if _connections_node.get_child_count() == 0:
			_confirm_button.set_disabled(true)
		_hovered_connection = null


func _on_star_entered(__star_node: Area2D) -> void:
	if __star_node.is_active():
		__star_node.highlight()
		_hovered_star = __star_node


func _on_star_exited(__star_node: Area2D) -> void:
	if __star_node.is_active():
		__star_node.unhighlight()
		_hovered_star = null


func _on_star_input_event(__viewport: Node, __event: InputEvent, __shape_idx: int, __star_node: Area2D) -> void:
	if not __star_node.is_active():
		return
	
	if __event is InputEventMouseButton and Input.is_action_just_pressed('click_star'):
		_line.start = __star_node.get_position()
		_line.start_node = __star_node
		_line.is_drawing = true
	elif (__event is InputEventMouseButton and Input.is_action_just_released('click_star') and
		__star_node != _line.start_node):
		for __connection in _connections:
			if _line.start_node in __connection and __star_node in __connection:
				__reset_line()
				return
		var __new_line = __add_line(__star_node)
		_connections.append([_line.start_node, __star_node, __new_line])
		__reset_line()
	else:
		__reset_line()


func _draw() -> void:
	if _line.is_drawing:
		draw_line(_line.start, _line.end, _LINE_COLOR, _LINE_WIDTH)


func __reset_line() -> void:
	_line.is_drawing = false
	_line.start = Vector2.ZERO
	_line.end = Vector2.ZERO
	queue_redraw()


func __add_line(__star_node: Area2D) -> Line2D:
	var __angle_start_to_end = Vector2(0.0, -48.0).angle_to(__star_node.get_position() - _line.start)
	var __line_start: Vector2 = Vector2(0.0, -48.0).rotated(__angle_start_to_end) + _line.start
	var __line_end: Vector2 = Vector2(0.0, 48.0).rotated(__angle_start_to_end) + __star_node.get_position()
	
	var __new_line := Line2D.new()
	__new_line.add_point(__line_start)
	__new_line.add_point(__line_end)
	__new_line.set_default_color(_LINE_COLOR)
	__new_line.set_width(_LINE_WIDTH)
	_connections_node.add_child(__new_line)
	if _connections_node.get_child_count() == 1:
		_confirm_button.set_disabled(false)
	return __new_line
