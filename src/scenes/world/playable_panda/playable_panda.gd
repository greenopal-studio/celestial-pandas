extends CharacterBody3D


const SPEED = 2.0
const SPEED_SPRINT = SPEED * 1.4
const MOUSE_SPEED = 0.0025
const GRAVITY = 10.0
const ROTATE_MAX_RANGE_Y = deg_to_rad(30.0)
const RAY_LENGTH = 1.0
const START_POS = Vector3(-0.668, 0, -4.068)


@onready var _camera = $Camera
@onready var _inventory = %Inventory
@onready var _thoughts = %ThoughtHud
@onready var _canvas_layer = $CanvasLayer
@onready var _crosshair = $CanvasLayer/CrosshairContainer
@onready var _star_view = $CanvasLayer/SubViewportContainer
@onready var _animation_player := $AnimationPlayer
@onready var _day_number_label := $CanvasLayer/ColorRect/DayNumber


var _mouse_movement = Vector2.ZERO
var _last_target = null
var _is_inventory_open := false
var _is_star_view_open := false
var _is_in_thought := false


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	%Panda.hide()
	
	STATE.start_star_view.connect(_on_start_star_view)
	STATE.leave_star_view.connect(_on_leave_star_view)
	STATE.set_gui_visible.connect(_on_set_gui_visible)
	
	STATE.end_day.connect(_on_end_day)

	STATE.add_hint_thoughts.connect(_on_thought)
	STATE.add_dialogue_thoughts.connect(_on_thought)

	STATE.done_with_thoughts.connect(_on_done_with_thoughts)


func _on_start_star_view() -> void:
	_star_view.show()
	_is_star_view_open = true


func _on_leave_star_view(__confirmed: bool, __constellation_id: ID.CONSTELLATION_TYPES, __items: Array) -> void:
	_star_view.hide()
	_is_star_view_open = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _on_set_gui_visible(__is_visible: bool) -> void:
	_canvas_layer.set_visible(__is_visible)


func _on_thought(__x) -> void:
	_crosshair.hide()
	_is_in_thought = true


func _on_done_with_thoughts() -> void:
	_crosshair.show()
	_is_in_thought = false


func _on_end_day() -> void:
	_animation_player.play('start_new_day')
	await get_tree().create_timer(1.35).timeout
	_day_number_label.set_text(str(STATE.get_day()))


func _input(__event: InputEvent):
	if _is_star_view_open:
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	elif _is_inventory_open:
		if __event is InputEventKey and Input.is_action_just_pressed('toggle_inventory'):
			_inventory.deactivate()
			_is_inventory_open = false
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

			await get_tree().create_timer(0.25).timeout
			_thoughts.show()

	elif _is_in_thought:
		pass

	else:
		if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
			# This needs to be done in _input because _ready is too early in the HTML5 export
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		if __event is InputEventMouseMotion:
			_mouse_movement -= __event.get_relative() * MOUSE_SPEED
			_mouse_movement.y = clamp(_mouse_movement.y, ROTATE_MAX_RANGE_Y * -1.0, ROTATE_MAX_RANGE_Y)
			_camera.transform.basis = Basis() # reset rotation
			_camera.rotate_object_local(Vector3(0, 1, 0), _mouse_movement.x) # first rotate in Y
			_camera.rotate_object_local(Vector3(1, 0, 0), _mouse_movement.y) # then rotate in X
		
		elif __event is InputEventKey and Input.is_action_just_pressed('toggle_inventory'):
			_thoughts.hide()

			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

			_inventory.activate()
			_is_inventory_open = true

		elif (
			_last_target
			and __event is InputEventKey and Input.is_action_just_pressed(_last_target.get_action_name())
		):
			if 'handle_interaction_with_player' in _last_target:
				_last_target.handle_interaction_with_player(position)
			else:
				_last_target.handle_interaction()
		elif __event is InputEventKey and Input.is_action_just_pressed('reset_pos'):
			set_position(START_POS)


func _physics_process(__delta: float):
	if (
		_is_inventory_open
		or _is_star_view_open
		or _is_in_thought
	):
		return

	__move(__delta)

	__intersect_view_ray()


func __move(__delta: float) -> void:	
	if not is_on_floor():
		velocity.y -= GRAVITY * __delta

	var input_dir = Input.get_vector(
		"camera_move_left",
		"camera_move_right",
		"camera_move_forward",
		"camera_move_backward"
	)
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if Input.is_action_pressed('camera_sprint'):
		direction *= SPEED_SPRINT
	else:
		direction *= SPEED
	
	velocity.x = direction.x
	velocity.z = direction.z

	velocity = velocity.rotated(Vector3(0, 1, 0), _mouse_movement.x)
	
	move_and_slide()
	
	if not __is_on_top_of_habitat():
		position -= get_position_delta()
		velocity = Vector3.ZERO


## This method needs to be called from inside _physics_process as the ray-cast uses the physics
## space state, which may be locked inside _input!
func __intersect_view_ray() -> void:
	var center_point = _camera.get_viewport().get_visible_rect().get_center()
	var origin = _camera.project_ray_origin(center_point)
	var end = origin + _camera.project_ray_normal(center_point) * RAY_LENGTH
	
	var query = PhysicsRayQueryParameters3D.create(
		origin, end, 2
	)
	query.collide_with_areas = true
	
	var space_state = get_world_3d().direct_space_state
	var collision = space_state.intersect_ray(query)
	
	var target = null
	if 'collider' in collision:
		var collider = collision['collider']
		var possible_target = collider
		while 'is_interactable' not in possible_target:
			possible_target = possible_target.get_parent()
		if possible_target.is_interactable():
			target = possible_target
	
	if target != _last_target:
		if _last_target:
			_last_target.unfocus()
			_thoughts.clear_input_thought()

		if target:
			target.focus()
			_thoughts.set_input_thought(
				target.get_action_name(),
				target.marker_color,
				target.thought_text
			)
			_thoughts.show()
			
		_last_target = target


func __is_on_top_of_habitat() -> bool:
	var origin = position + Vector3(0, safe_margin, 0)
	var end = position + Vector3(0, -2, 0)
	
	var query = PhysicsRayQueryParameters3D.create(
		origin, end, 1, [get_rid()]
	)
	query.collide_with_bodies = true
	
	var space_state = get_world_3d().direct_space_state
	var collision = space_state.intersect_ray(query)

	return 'collider' in collision
