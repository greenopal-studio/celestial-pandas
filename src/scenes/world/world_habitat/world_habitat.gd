extends Node3D


@onready var _juicy_island: Node3D = %JuicyIsland
@onready var _desert_island: Node3D = %DesertIsland
@onready var _final_island: Node3D = %FinalIsland
@onready var _shiny_island: Node3D = %ShinyIsland


func _ready():
	STATE.constellation_summoned.connect(_on_constellation_summoned)

	DEBUG.add_action_button('Show all islands', _on_debug_show_all)

	_juicy_island.hide()
	_desert_island.hide()
	_final_island.hide()
	_shiny_island.hide()


func _on_constellation_summoned(__constellation_id: ID.CONSTELLATION_TYPES) -> void:
	match __constellation_id:
		ID.CONSTELLATION_TYPES.CONSTELLATION_JUICY:
			_do_it(_juicy_island)
		ID.CONSTELLATION_TYPES.CONSTELLATION_DESERT:
			_do_it(_desert_island)
		ID.CONSTELLATION_TYPES.CONSTELLATION_SHINY:
			_do_it(_shiny_island)
		ID.CONSTELLATION_TYPES.CONSTELLATION_FINAL:
			_do_it(_final_island)


func _on_debug_show_all() -> void:
	_juicy_island.show()
	_desert_island.show()
	_final_island.show()
	_shiny_island.show()


func _do_it(__island: Node) -> void:
	__island.play_spawn_animation()
	await get_tree().process_frame
	__island.show()
