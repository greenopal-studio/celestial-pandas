extends Node3D


@onready var _anim: AnimationPlayer = $AnimationPlayer


func _ready():
	visibility_changed.connect(_on_visibility_changed)


func _on_visibility_changed() -> void:
	# the islands have a static collider
	# when their are hidden, we also need to disable it, otherwise the player can still walk on top
	#  of the collider (visible only affects rendering display)
	if visible:
		process_mode = Node.PROCESS_MODE_INHERIT
	else:
		process_mode = Node.PROCESS_MODE_DISABLED


func play_spawn_animation() -> void:
	_anim.play('spawn')
