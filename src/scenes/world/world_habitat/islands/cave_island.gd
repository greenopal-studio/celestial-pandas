extends Node3D


const ACTIVE_SPRITE_OPACITY = 1.0
const INACTIVE_SPRITE_OPACITY = 0.1
const INACTIVE_SPRITE_COLOR = Color(1, 1, 1, 0.1)
const WRONG_SPRITE_COLOR = Color.CRIMSON


@onready var _constellation_groups: Dictionary = {
	ID.CONSTELLATION_TYPES.CONSTELLATION_DESERT: $Constellation1,
	ID.CONSTELLATION_TYPES.CONSTELLATION_JUICY: $Constellation2,
	ID.CONSTELLATION_TYPES.CONSTELLATION_SHINY: $Constellation3,
	ID.CONSTELLATION_TYPES.CONSTELLATION_FINAL: $Constellation4
}
@onready var _camera_node: Camera3D = $Camera3D
@onready var _sacrifice_table := $SacrificeTable
@onready var _animation_player := $AnimationPlayer


var _constellation_id := ID.CONSTELLATION_TYPES._NONE


func _ready() -> void:
	STATE.leave_star_view.connect(_on_leave_star_view)
	
	_animation_player.animation_finished.connect(_on_animation_ended)


func _on_leave_star_view(__confirmed: bool, __constellation_id: ID.CONSTELLATION_TYPES, __items: Array) -> void:
	if not __confirmed:
		return
	
	_constellation_id = __constellation_id
	STATE.emit_signal('set_gui_visible', false)
	_camera_node.set_current(true)
	var __pos_1 = [1, 2, 3, 4, 5]
	var __pos_2 = [6, 7, 8, 9]
	__pos_1.shuffle()
	__pos_2.shuffle()
	for __i in __items.size():
		await get_tree().create_timer(0.25).timeout
		if __i < __pos_1.size():
			_sacrifice_table.put_item_on_table(__items[__i], __pos_1[__i])
		else:
			_sacrifice_table.put_item_on_table(__items[__i], __pos_2[__i - __pos_1.size()])
	
	await get_tree().create_timer(1.0).timeout
	
	_animation_player.play('move_camera')


func _on_animation_ended(__anim_name: StringName) -> void:
	if __anim_name == 'RESET':
		return
	
	if __anim_name == 'shake_camera':
		_camera_node.set_current(false)
		STATE.emit_signal('constellation_summoned', _constellation_id)
		_animation_player.play('RESET')
	elif __anim_name == 'move_camera':
		if _constellation_id != ID.CONSTELLATION_TYPES._NONE:
			activate_constellation(_constellation_id)
			await get_tree().create_timer(
				0.5 * _constellation_groups[_constellation_id].get_child_count() + 0.5
			).timeout
			_animation_player.play('shake_camera')
		else:
			var __constellation_state = STATE.get_constellation_state()
			for __constellation_id in _constellation_groups:
				if not __constellation_state[__constellation_id]:
					wrong_combination(__constellation_id)
					break
			await get_tree().create_timer(2.0).timeout
			STATE.emit_signal('set_sacrifice_tries', STATE.get_sacrifice_tries() + 1)
			_camera_node.set_current(false)
			STATE.emit_signal('set_gui_visible', true)
			_sacrifice_table.reset()
			_animation_player.play('RESET')
			STATE.emit_signal('end_day')


func activate_constellation(__constellation_type: ID.CONSTELLATION_TYPES) -> void:
	for __child in _constellation_groups[__constellation_type].get_children():
		await get_tree().create_timer(0.5).timeout
		__child.modulate.a = ACTIVE_SPRITE_OPACITY


func wrong_combination(__constellation_type: ID.CONSTELLATION_TYPES) -> void:
	for __i in 3:
		for __child in _constellation_groups[__constellation_type].get_children():
			__child.modulate = WRONG_SPRITE_COLOR
		await get_tree().create_timer(0.5).timeout
		for __child in _constellation_groups[__constellation_type].get_children():
			__child.modulate = INACTIVE_SPRITE_COLOR
		await get_tree().create_timer(0.1).timeout
