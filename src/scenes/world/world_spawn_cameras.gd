extends Node3D


const ANIM_DURATION = 8.0


@onready var _juicy_cam: Camera3D = $JuicySpawnCamera
@onready var _desert_cam: Camera3D = $DesertSpawnCamera
@onready var _shiny_cam: Camera3D = $ShinySpawnCamera
@onready var _final_cam: Camera3D = $FinalSpawnCamera


func _ready():
	STATE.constellation_summoned.connect(_on_constellation_summoned)


func _on_constellation_summoned(__constellation_id: ID.CONSTELLATION_TYPES) -> void:
	match __constellation_id:
		ID.CONSTELLATION_TYPES.CONSTELLATION_JUICY:
			_do_it(_juicy_cam)
		ID.CONSTELLATION_TYPES.CONSTELLATION_DESERT:
			_do_it(_desert_cam)
		ID.CONSTELLATION_TYPES.CONSTELLATION_SHINY:
			_do_it(_shiny_cam)
		ID.CONSTELLATION_TYPES.CONSTELLATION_FINAL:
			_do_it(_final_cam)


func _do_it(__cam: Camera3D) -> void:
	STATE.set_gui_visible.emit(false)
	__cam.set_current(true)

	await get_tree().create_timer(ANIM_DURATION).timeout

	__cam.set_current(false)
	
	STATE.set_gui_visible.emit(true)
	STATE.emit_signal('set_sacrifice_tries', 0)
	STATE.emit_signal('end_day')
