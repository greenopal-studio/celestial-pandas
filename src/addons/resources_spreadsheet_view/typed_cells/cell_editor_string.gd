extends ResourceTablesCellEditor


func create_cell(caller : Control) -> Control:
	var node = load(CELL_SCENE_DIR + "string.tscn").instantiate()
	return node


func is_text():
	return false


func set_color(node : Control, color : Color):
	node.get_node("Back").modulate = color * 0.6 if !node.text.is_valid_float() else color
