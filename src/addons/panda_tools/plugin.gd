@tool
extends EditorPlugin


const _panel_scene = preload('res://addons/panda_tools/panel/panel.tscn')


var _panel_instance: Node


func _enter_tree():
	if not Engine.is_editor_hint():
		return
		
	_panel_instance = _panel_scene.instantiate()
	EditorInterface.get_editor_main_screen().add_child(_panel_instance)
	_make_visible(false)


func _exit_tree():
	if _panel_instance:
		_panel_instance.queue_free()


func _has_main_screen():
	return true


func _make_visible(visible):
	if _panel_instance:
		_panel_instance.visible = visible


func _get_plugin_name():
	return "Data"


func _get_plugin_icon():
	return EditorInterface.get_editor_theme().get_icon("GDScript", "EditorIcons")
