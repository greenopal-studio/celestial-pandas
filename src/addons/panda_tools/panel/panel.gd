@tool
extends Control


@onready var _buttons_container = %ButtonsContainer


var _file_dialog: FileDialog


func _ready():
	#todo
	_create_button('Items', 'items', Item)
	_create_button('Islands', 'islands', Island)
	_create_button('Constellations', 'constellations', Constellation)

	_file_dialog = FileDialog.new()
	_file_dialog.access = FileDialog.ACCESS_RESOURCES
	_file_dialog.file_mode = FileDialog.FILE_MODE_SAVE_FILE
	_file_dialog.add_filter('*.tres', 'Resource')
	_file_dialog.canceled.connect(_on_file_canceled)
	add_child(_file_dialog)


func _create_button(__display_name: String, __folder_name: String, __class) -> void:
	var button = Button.new()
	button.set_text('Create new %s'%__display_name)
	button.icon = EditorInterface.get_editor_theme().get_icon("Add", "EditorIcons")
	_buttons_container.add_child(button)
	button.pressed.connect(_on_button_pressed.bind(__folder_name, __class))


func _on_button_pressed(__folder_name: String, __class) -> void:
	var full_path = DATA.BASE_PATH + '/' + __folder_name + '/'
	_file_dialog.set_current_path(full_path)

	_file_dialog.file_selected.connect(_on_file_selected.bind(__class), CONNECT_ONE_SHOT)

	_file_dialog.popup_centered(Vector2(512, 512))


func _on_file_selected(__path: String, __class) -> void:
	var resource: Resource = __class.new()
	resource.resource_path = __path
	ResourceSaver.save(resource, __path)

	EditorInterface.edit_resource(resource)


func _on_file_canceled() -> void:
	for connection in _file_dialog.file_selected.get_connections():
		_file_dialog.file_selected.disconnect(connection.callable)
